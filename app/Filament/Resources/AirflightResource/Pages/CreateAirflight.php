<?php

namespace App\Filament\Resources\AirflightResource\Pages;

use App\Filament\Resources\AirflightResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAirflight extends CreateRecord
{
    protected static string $resource = AirflightResource::class;
}
