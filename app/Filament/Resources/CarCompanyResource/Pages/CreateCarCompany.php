<?php

namespace App\Filament\Resources\CarCompanyResource\Pages;

use App\Filament\Resources\CarCompanyResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCarCompany extends CreateRecord
{
    protected static string $resource = CarCompanyResource::class;
}
