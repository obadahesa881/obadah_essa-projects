<?php

namespace App\Filament\Resources\CarUserResource\Pages;

use App\Filament\Resources\CarUserResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCarUser extends CreateRecord
{
    protected static string $resource = CarUserResource::class;
}
