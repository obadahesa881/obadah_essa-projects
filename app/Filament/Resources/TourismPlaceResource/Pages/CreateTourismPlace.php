<?php

namespace App\Filament\Resources\TourismPlaceResource\Pages;

use App\Filament\Resources\TourismPlaceResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTourismPlace extends CreateRecord
{
    protected static string $resource = TourismPlaceResource::class;
}
