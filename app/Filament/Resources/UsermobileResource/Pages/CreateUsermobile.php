<?php

namespace App\Filament\Resources\UsermobileResource\Pages;

use App\Filament\Resources\UsermobileResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateUsermobile extends CreateRecord
{
    protected static string $resource = UsermobileResource::class;
}
