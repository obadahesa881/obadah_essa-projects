<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('usermobiles', function (Blueprint $table) {
            $table->id();
            $table->string('full_name')->virtualAs('concat(name, \' \', last_name)');
            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('gender')->nullable();
            $table->string('google_id')->nullable();
            $table->integer('resetPasswordCode')->nullable();
            $table->integer('verifiedAccountCode')->nullable();
            $table->boolean('isVerified')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('usermobiles', function (Blueprint $table) {
            $table->dropColumn('google_id');
        });
    }
};
